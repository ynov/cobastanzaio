const StanzaIO = require('stanza.io')

console.log('hello, world')
console.log(StanzaIO)

const client = StanzaIO.createClient({
  jid: 'foo@example.com',
  password: 'password',
  transport: 'websocket',
   wsURL: 'wss://example.com:5281/xmpp-websocket'
})

client.on('session:started', () => {
  console.log('Session started!')
})

client.connect()
