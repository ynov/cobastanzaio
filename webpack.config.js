const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: {
    main: './index.js'
  },
  resolve: {
    extensions: [ '.js' ],
  },
  output: {
    path: __dirname,
    filename: 'bundle.js'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        use: [ 'source-map-loader' ]
      }
    ]
  },
  plugins: []
}
